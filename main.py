import sys
import argparse
import threading
from module_Malshare.h_malshare import Malshare
from module_Virus_Share.h_virus_share import Virus_Share
from module_Domain.d_list import Domains

def virus_share_module():         
    try:
        h_Virus_Share = Virus_Share()
        h_Virus_Share.check_file_exist()
        last_line = h_Virus_Share.get_last_day()
        print(h_Virus_Share.get_content(last_line))
        
    except KeyboardInterrupt:
        print("\nExitting Program !!!!")
        sys.exit()

def malshare_module():     
    h_Malshare = Malshare()         
    h_Malshare.check_file_exist()   
    try:         
        h_Malshare.last_day()
        print(h_Malshare.get_content())

    except KeyboardInterrupt:
        print("\nExitting Program !!!!")
        sys.exit()
def new_list_module(url):          
    try:         
        Domains.scrap_domain(url) 

    except KeyboardInterrupt:
        print("\n[*] Exitting Program !")
        sys.exit()

def all_list_module():          
    try:         
        Domains.scrap_domain_auto(0)

    except KeyboardInterrupt:
        print("\n[*] Exitting Program !")
        sys.exit()

def check_domain_module(url):          
    try:         
        Domains.analyze_domain(url)

    except KeyboardInterrupt:
        print("\n[*] Exitting Program !")
        sys.exit()


# MAIN FUNCTION THAT CALLS MODULE 
def main():
    print(r"""
   ___                __    _______        _    
  / _ \__ _________  / /__ / ___/ /  ___ _(_)__ 
 / ___/ // / __/ _ \/ / -_) /__/ _ \/ _ `/ / _ \
/_/   \_,_/_/ / .__/_/\__/\___/_//_/\_,_/_/_//_/
             /_/                                
            Authors : PurpleChain_Team : DS, HS, CW, MP 
            """)

    parser = argparse.ArgumentParser()
    parser.add_argument("--hashes", help="Scrap the Hashes | HASHES (malshare,virusshare)", type=str)
    parser.add_argument("--all_list", help  ="[*] Usage : all_list", action='store_true')
    parser.add_argument("--new_list", help  ="[*] Usage : --new_list [URL]" )
    parser.add_argument("--check_domain", help  ="[*] Usage : --check_domain [URL]")
    args = parser.parse_args()

    if not any(vars(args).values()):
        parser.print_help()
        print("\n[*] Exitting Program !")
        sys.exit()

    if args.all_list:
        print("[+] ALL LIST MODULE - DOMAINS")
        all_list_module()
    elif args.new_list:
        print("[+] NEW LIST MODULE - DOMAINS")
        new_list_module(args.new_list)
    elif args.check_domain :
        print("[+] CHECK DOMAIN MODULE - DOMAINS")
        check_domain_module(args.check_domain)

    elif args.hashes == "malshare":
        print("[+] MALSHARE MODULE - HASHES")
        malshare_module()

    elif args.hashes == "virusshare":
        print("[+] VIRUS SHARE MODULE - HASHES")
        virus_share_module()

    elif args.hashes == "A":
        print("[+] SCRAPE ALL THE HASHES - THREAD OPTIONS")
        try:         
            t1 = threading.Thread(target=malshare_module)     
            t2 = threading.Thread(target=virus_share_module)
            t1.start()
            t2.start()
            t1.join()
            t2.join()
        except KeyboardInterrupt:            
            print("\nExitting Program !!!!")           
            sys.exit()


if __name__ == "__main__": 
    main()