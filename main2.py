#!/bin/env python3 

from flask import Flask, render_template, request

app = Flask(__name__)


###############################################
#          Render live page                   #
###############################################
@app.route('/', methods=["GET"])
def get_live():
    return(render_template('index.html'))

    
###############################################
#          Handler Error : 404 page    v      #
###############################################
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html')

if __name__ == "__main__":
    app.run(host='localhost', port=5000, debug=True)
