#-*-coding:Latin-1 -* 
import os,urllib
import os.path
import urllib.request
from pymongo import MongoClient
import numpy as np
import joblib
import threading
# Import libraries

from .phishingdetection import getFeatures
# Import function from phishingdetection

class Domains(): 

    CLASSIFIER_LOCATION = os.path.join(os.getcwd(), 'module_Domain/random_forest.pkl')

    def find_method(given, sub):
        return given.find(sub)
    # Function that can find string given

    def get_database():
        #conn = MongoClient("mongodb://admin:admin@cluster0-shard-00-00.v8txs.mongodb.net:27017,cluster0-shard-00-01.v8txs.mongodb.net:27017,cluster0-shard-00-02.v8txs.mongodb.net:27017/purple?ssl=true&replicaSet=atlas-hoxgja-shard-0&authSource=admin&retryWrites=true&w=majority") 
        conn = MongoClient('localhost', 27017)
        return conn['purple']
    # Create a connection using MongoClient.

    def open_file(file,type) :
        file_open = open(file, type)
        lines_open = file_open.readlines()
        return lines_open
    # Open file and return lines

    def analyze_domain(url1):
        data = {} 
        # Create tab

        if not (url1.startswith("https://") or url1.startswith("http://")):
            if url1.startswith("www."):
                url = "https://" + url1
            else:
                url = "https://www." + url1
        # If the url given has not "https or http" add one

        features_test1 = getFeatures(url)
        # Call the feature to scan the domain

        features_test = np.array(features_test1).reshape((1, -1))
        # Put in array the result

        clf = joblib.load(Domains.CLASSIFIER_LOCATION)
        # Call the random forest for the machine learning

        prediction = int(clf.predict(features_test)[0])
        # Calcul the prediction

        if prediction == 1:
            print("[*] "+url1+" : SAFE")
        else:   
            data = {'domain': url1}
            # Put the data in json format

            print("[+] "+url1+" : UNSAFE | ADDED IN THE DATABASE ")

            db = Domains.get_database()
            collection = db.data
            collection.insert_one(data)
            # Insert the data in the database

    def find_domain(Lines) :
        
        count = 0
        for line in Lines:
            comment = Domains.find_method(line.strip(), '#')
            if line == "\n" :
                empty = 0
            else :
                empty = -1

            if comment == -1 and empty == -1: 
                good127 = Domains.find_method(line.strip(), '127.0.0.1')
                local = Domains.find_method(line.strip(), 'localhost')
                good0 = Domains.find_method(line.strip(), '0.0.0.0')

                if local == -1 and good127 == 0 :
                    count += 1
                    new_set = line.replace('127.0.0.1', '')
                    replace = new_set.replace('\n','')
                    update = Domains.check_update(replace)
                    if update == 0 :
                        print("[*] This domain : "+replace+" is already in the database")
                    else :
                        Domains.analyze_domain(replace)

                if good0 == 0 :
                    count += 1
                    new_set = line.replace('0.0.0.0', '')
                    replace = new_set.replace('\n','')
                    update = Domains.check_update(replace)
                    if update == 0 :
                        print("[*] This domain : "+replace+" is already in the database")
                    else :
                        Domains.analyze_domain(replace)
                
                if good0 != 0 and good127 != 0 : 
                    count += 1
                    update = Domains.check_update(line.strip())
                    if update == 0 :
                        print("[*] This domain : "+line.strip()+" is already in the database")
                    else :
                        Domains.analyze_domain(line.strip())
                    
    def check_update(url) :
        db = Domains.get_database()
        collection = db.data

        if collection.count_documents({ 'domain': url }):
            return 0 # already in the database
        else:
            return 1 # Add in the database

    def scrap_domain(lien) : # Add new list 

        Lined = Domains.open_file('/domain_liste.txt', 'r')
        # Open and read all the list already exists

        new_lien = lien + "\n" # Line break

        if new_lien in Lined : # If the list already exist
            print("[*] List already in the database")
            exit()
        else :
            file2 = open('module_Domain/domain_liste.txt', 'a')
            file2.write(new_lien)

        name = os.path.splitext(os.path.basename(urllib.parse.urlsplit(lien).path))
        new_name = name[0]
        # Get the name of the file

        urllib.request.urlretrieve (lien, new_name)
        # Donwload the file and set the name

        Lines = Domains.open_file(new_name,'r')

        Domains.find_domain(Lines)
        
        os.remove(new_name)

    def scrap_domain_auto(new_count) : 

        Lined = Domains.open_file('module_Domain/domain_liste.txt', 'r')

        countd = new_count

        while countd != len(Lined) :

            actual_domain = Lined[countd]

            name = os.path.splitext(os.path.basename(urllib.parse.urlsplit(actual_domain).path))

            new_name = name[0]

            urllib.request.urlretrieve (actual_domain, new_name)

            Lines = Domains.open_file(new_name,'r')

            CHUNKS = 15
            CHUNK_SIZE = int(len(Lines) / CHUNKS)

            threads = []
            for i in range(0, CHUNKS) :
                start = i*CHUNK_SIZE
                if i == CHUNKS-1 :
                    end = len(Lines)
                else :
                    end = i*CHUNK_SIZE + CHUNK_SIZE
                chunk = Lines[start:end]
                threads.append(threading.Thread(target=Domains.find_domain(Lines), args=(chunk,)))

            # Démarrer les threads
            for thread in threads :
                thread.start()

            # Attendre la fin des threads
            for thread in threads :
                thread.join()

            countd += 1

            os.remove(new_name)
