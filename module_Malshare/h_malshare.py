import os 
import json
from datetime import date, timedelta
import requests
from pymongo import MongoClient

# TO DO - Rajouter un argument "upload"

# Template : https://malshare.com/daily/2017-09-26/malshare_fileList.2017-09-26.all.txt

class Parsing(): 
    # Parsing each file into the JSON file 
    def parse_input_to_json(self,date): 
        hashes = ["md5", "sha1", "sha256", "ssdeep"]
        with open(f"module_Malshare/date.txt", 'r', encoding='utf-8') as file_input:
            with open('module_Malshare/json.txt','a', encoding='utf-8') as writing_file:
                for line in file_input:
                    line = line.rstrip().split()        
                    dictionnary = json.dumps(dict(zip(hashes,line)))
                    writing_file.write(dictionnary) 
    

class Malshare():

    # Check if the file "date.txt" exists    
    def check_file_exist(self):
        if os.path.isfile('module_Malshare/date.txt'):
            return True 
        else:
            file = open("module_Malshare/date.txt", "a")
            file.write("2018-07-15")
            file.close()


    # Get the last day in the file "date.txt"
    def last_day(self):
        FILE = "module_Malshare/date.txt"
        with open(FILE, 'r', encoding='utf-8') as file_input:        
            last_line = file_input.readlines()[-1]
            last_line = last_line.rstrip("\n")
            last_day = last_line  
            self.year = last_day[0:4]    
            self.month = last_day[5:7]    
            self.day = last_day[8:10]
            # return int(year), int(month), int(day)   

    def get_content(self):
        h_Parsing = Parsing()     
        base = "https://malshare.com/daily/"
        url_param_craft = str(self.year)+"-"+str(self.month)+"-"+str(self.day)
        actual_date = str(date.today())
        actual_year, actual_month, actual_day= actual_date[0:4], actual_date[5:7], actual_date[8:10]
        url_crafted = actual_year + "-" + actual_month + "-" + actual_day
        if url_crafted == url_param_craft:
            print("[+] You are up to date !")
            
        else:
            print("[-] You are not up to date ! Scrapper Update !")        
            sdate = date(int(self.year), int(self.month), int(self.day))   # Starting Date
            edate = date(int(actual_year), int(actual_month), int(actual_day))   # Ending Date

            delta = edate - sdate

            for i in range(delta.days):
                final_date = str(sdate + timedelta(days=i))
                final_request = base + str(final_date) + "/malshare_fileList." + str(final_date) + ".all.txt"
                print("[+] " + final_request)
                r = requests.get(final_request)       
                f = open(f"module_Malshare/{final_date}", "w")
                f.write(r.text)
                h_Parsing.parse_input_to_json(final_date)
                f.close()
                d = open("module_Malshare/date.txt","a")
                d.write(final_date+"\n")               
        print("[+] Scraper is up to date")




