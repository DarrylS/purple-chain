import requests
import json
import os
from pymongo import MongoClient

# Template = https://virusshare.com/hashfiles/VirusShare_00004.md5

class Parsing(): 

    def parse_input_into_json(self,num):     
        FILE = str(num)
        # print(FILE)
        HASHES = ["md5"]
        with open(f'module_Virus_Share/'+FILE, 'r', encoding='utf-8') as file_input:
            file_input = file_input.readlines()[6:]
            with open('module_Virus_Share/json.txt','a') as writing_file:            
                for line in file_input:
                    line = line.rstrip().split()        
                    dictionnary = json.dumps(dict(zip(HASHES,line)))
                    writing_file.write(dictionnary+"\n")
                

class Virus_Share():
    
    # Check if the file "date.txt" exists    
    def check_file_exist(self):
        if os.path.isfile('module_Virus_Share/num.txt'):
            return True 
        else:
            file = open("module_Virus_Share/num.txt", "a")
            file.write("0\n")
            file.close()

    def get_last_day(self):
        FILE = "module_Virus_Share/num.txt"
        with open(FILE, 'r', encoding='utf-8') as file_input:        
            last_line = file_input.readlines()[-1]
            last_line = last_line.rstrip("\n")
            return int(last_line)


    def get_content(self,last_line): 
        h_Parsing = Parsing()
        i = last_line + 1
        while i < 391: 
            if i<10:        
                number = "0000"+str(i)
            elif i<100:
                number = "000"+str(i)
            else:        
                number = "00"+str(i)

            URL  = "https://virusshare.com/hashfiles/VirusShare_"+number+".md5"

            r = requests.get(URL)

            if r.status_code == 200:
                print("OK ! Get the file :", URL)         
                f = open(f"module_Virus_Share/"+str(i), 'w')
                f.write(r.text)
                f.close()
                f1 = open("module_Virus_Share/num.txt", 'a')
                f1.write(str(i)+"\n")
                h_Parsing.parse_input_into_json(str(i))
                i += 1
                
            else:
                print("File does not exist !")
